## How to Run

To run the Employee Management System application, follow these steps:

1. **Clone Repository**: 
    - Clone this repository to your local machine using the following command:
    ```
    git clone https://gitlab.com/rofinurhakim/Angular-test.git
    ```

2. **Install Dependencies**:
    - Navigate to the project directory and install the necessary dependencies by running:
    ```
    npm install
    ```

3. **Run the Application**:
    - After installing dependencies, start the Angular development server by running:
    ```
    ng serve
    ```
    This will compile the application and host it on a local server. You can access the application in your web browser at `http://localhost:4200`.

## Coding Process

The coding process for the Employee Management System involved several stages:

1. **Planning and Design**:
    - Initially, the team discussed the requirements and functionalities of the system.
    - Wireframes and mockups were created to visualize the layout and user interface of the application.

2. **Setting Up Angular Project**:
    - The Angular project was set up using the Angular CLI.
    - Dependencies such as Angular Material for UI components and other required libraries were installed.

3. **Implementing Pages**:
    - Each page of the application was implemented according to the specifications:
        - Login Page
        - Employee List Page
        - Add Employee Page
        - Employee Detail Page
    - Components, services, and routing were set up for navigation and data management between pages.

4. **Adding Functionality**:
    - Functionality such as paging, sorting, searching, form validation, and data formatting was implemented.
    - Angular features like reactive forms, routing, and HTTP client were utilized for data handling and user interaction.

5. **Styling and Theming**:
    - The application was styled using CSS and Angular Material theming.
    - Responsive design principles were applied to ensure compatibility across different devices and screen sizes.

6. **Testing and Debugging**:
    - The application was thoroughly tested to ensure all features work as expected.
    - Debugging was performed to address any issues or errors encountered during development.

7. **Optimization and Refinement**:
    - Code optimization and refactoring were done to improve performance and maintainability.
    - Feedback from testing and code reviews was incorporated to refine the application further.

Overall, the coding process involved careful planning, implementation of features, testing, and refinement to create a robust and user-friendly Employee Management System using Angular.
